package com.weathen.cureapp

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.weathen.cureapp.contacts.activity.MainActivity
import com.weathen.cureapp.contacts.view.ContactHolder

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.weathen.cureapp", appContext.packageName)
    }

    @get:Rule
    var testRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun test_contactDetailsShowed_onItemClick() {
        Espresso.onView(ViewMatchers.withId(R.id.contact_list)).perform(RecyclerViewActions.actionOnItemAtPosition<ContactHolder>(1, click()))
        Espresso.onView(ViewMatchers.withId(R.id.image_view)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}
