package com.weathen.cureapp.model

import java.io.Serializable


data class Contact(
    val id: Long,
    val lookupKey: String,
    val name: String,
    val number: String?,
    val photoUri: String?
) : Serializable