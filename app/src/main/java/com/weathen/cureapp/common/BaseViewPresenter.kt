package com.weathen.cureapp.common

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ViewModel
import android.support.annotation.CallSuper

abstract class BaseViewPresenter<View>(var view: View?) : ViewModel(), LifecycleObserver {

    private var viewLifecycle: Lifecycle? = null

    open fun attachLifecycle(viewLifecycle: Lifecycle) {
        this.viewLifecycle = viewLifecycle
        viewLifecycle.addObserver(this)
    }

    protected fun view(): View? {
        return view
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onViewDestroyed() {
        view = null
        viewLifecycle = null
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
    }

}
