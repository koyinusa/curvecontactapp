package com.weathen.cureapp.common

import android.content.Context
import android.support.annotation.StringRes
import android.view.View

interface IBaseViewMvc {
    fun setRootView(view: View)
    fun getRootView() : View?
}

abstract class BaseViewMvc : IBaseViewMvc {

    var mRootView : View? = null

    override fun setRootView(view: View) {
        this.mRootView = view
    }

    override fun getRootView() : View? {
        return this.mRootView
    }

    fun getContext() : Context? {
        return this.mRootView?.context
    }

    fun <T: View> findViewById(id: Int) : T? {
        return this.mRootView?.findViewById(id)
    }

    fun getString(@StringRes stringResId: Int) : String? {
        return getContext()?.getString(stringResId)
    }
}
