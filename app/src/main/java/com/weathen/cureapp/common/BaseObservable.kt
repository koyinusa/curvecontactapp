package com.weathen.cureapp.common

import java.util.*


interface BaseObservableInt<LISTENER> {
    fun registerListener(listener: LISTENER)
    fun unregisterListener(listener: LISTENER)
}

abstract class BaseObservable<LISTENER> : BaseObservableInt<LISTENER>{

    var listener : MutableSet<LISTENER> = HashSet<LISTENER>()

    override fun registerListener(listener: LISTENER) {
        this.listener.add(listener)
    }

    override fun unregisterListener(listener: LISTENER) {
        this.listener.add(listener)
    }

    protected fun getListeners() : Set<LISTENER> {
        return Collections.unmodifiableSet(listener)
    }
}


abstract class BaseObservableViewMvc<LISTENER> : BaseViewMvc(), BaseObservableInt<LISTENER> {

    var listener : MutableSet<LISTENER> = HashSet<LISTENER>()

    override fun registerListener(listener: LISTENER) {
        this.listener.add(listener)
    }

    override fun unregisterListener(listener: LISTENER) {
        this.listener.add(listener)
    }

    fun getListeners() : Set<LISTENER> {
        return Collections.unmodifiableSet(listener)
    }
}
