package com.weathen.cureapp.details.view

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.weathen.cureapp.R
import com.weathen.cureapp.common.BaseViewMvc
import com.weathen.cureapp.model.Contact

class DetailsView(inflater: LayoutInflater, container: ViewGroup?) : BaseViewMvc() {

    private var mPhotoView: ImageView? = null
    private var mNameView: TextView? = null
    private var mNumberView: TextView? = null
    private var mCircleView: View? = null
    private var mFirstLetterTextView : TextView? = null

    init {
        setRootView(inflater.inflate(R.layout.activity_details, container, false))
        mPhotoView = findViewById(R.id.image_view)
        mNameView = findViewById(R.id.name)
        mNumberView = findViewById(R.id.number)
        mCircleView = findViewById(R.id.circle_composed_view)
        mFirstLetterTextView = findViewById(R.id.first_letter_view)
    }

    fun setContactDetails(details: Contact?) {
        mNameView?.text = details?.name

        mNumberView?.text = details?.number

        if (details?.photoUri != null) {
            mCircleView?.visibility = View.GONE
            Picasso.get().load(Uri.parse(details.photoUri)).into(mPhotoView)
        } else {
            mFirstLetterTextView?.text = "${details?.name?.first()}"
            mCircleView?.visibility = View.VISIBLE
        }

    }

}