package com.weathen.cureapp.details.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.weathen.cureapp.details.view.DetailsView
import com.weathen.cureapp.model.Contact

class DetailsActivity : AppCompatActivity() {

    companion object {
        var CONTACT_PARAM_EXTRA = "contact_param"
    }

    var mView: DetailsView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mView = DetailsView(layoutInflater, null);
        mView?.setContactDetails(getSchema())

        setContentView(mView?.getRootView())
    }

    private fun getSchema(): Contact? = intent.getSerializableExtra(CONTACT_PARAM_EXTRA) as? Contact

}
