package com.weathen.cureapp

import android.app.Application
import com.weathen.cureapp.di.component.AppComponent
import com.weathen.cureapp.di.component.DaggerAppComponent
import com.weathen.cureapp.di.modules.ContactModule

class Application : Application() {

    var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.
            builder().
            contactModule(ContactModule(this)).
            build()
    }


}