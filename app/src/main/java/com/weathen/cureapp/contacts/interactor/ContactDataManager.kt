package com.weathen.cureapp.contacts.interactor

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.weathen.cureapp.model.Contact

class ContactDataManager(private val dataSourceFacory : ContactsDataSourceFactory) {

    fun loadContacts() : LiveData<PagedList<Contact>> {
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setEnablePlaceholders(false)
            .build()
        return LivePagedListBuilder<Int, Contact>(dataSourceFacory, config).build()
    }

}


