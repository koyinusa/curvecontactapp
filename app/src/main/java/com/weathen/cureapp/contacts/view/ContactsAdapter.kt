package com.weathen.cureapp.contacts.view

import android.arch.paging.PagedListAdapter
import android.net.Uri
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.weathen.cureapp.R
import com.weathen.cureapp.model.Contact

class ContactsAdapter(diffCallback: DiffUtil.ItemCallback<Contact>) : PagedListAdapter<Contact, ContactHolder>(diffCallback) {

    var clickListener: ((Contact?) -> Unit)? = null
    var images = arrayOf(R.drawable.round_bg_default, R.drawable.round_bg_default_variant_one, R.drawable.round_bg_default_variant_two, R.drawable.round_bg_default_variant_three, R.drawable.round_bg_default_variant_four)

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): ContactHolder {
        val view = LayoutInflater.from(container.context).inflate(R.layout.list_contact, container, false)
        return ContactHolder(view)
    }

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        val contact = getItem(position)
        holder.bindData(contact)
        holder.circleView?.setBackgroundResource(images[position % 5])
        holder.itemView.setOnClickListener { clickListener?.invoke(contact) }
    }
}

class ContactHolder(view: View) : RecyclerView.ViewHolder(view) {

    val circleView: View? = this.itemView.findViewById(R.id.circle_view)
    private var firstLetterView: TextView = this.itemView.findViewById(R.id.first_letter_view)
    private var textView: TextView? = this.itemView.findViewById(R.id.name)
    private var photoView : ImageView? = this.itemView.findViewById(R.id.photo)

    fun bindData(contact: Contact?) {
        textView?.text = contact?.name
        firstLetterView.text = "${contact?.name?.first()}"

        if (contact?.photoUri != null){
            val uri = Uri.parse(contact.photoUri)
            Picasso.get().load(uri).into(photoView)
            circleView?.visibility = View.GONE
            firstLetterView?.visibility = View.GONE
        }
        else {
            circleView?.visibility = View.VISIBLE
            firstLetterView?.visibility = View.VISIBLE
        }
    }

}