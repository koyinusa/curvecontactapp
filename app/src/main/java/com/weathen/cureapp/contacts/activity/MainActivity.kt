package com.weathen.cureapp.contacts.activity

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.weathen.cureapp.Application
import com.weathen.cureapp.contacts.interactor.ContactDataManager
import com.weathen.cureapp.contacts.presenter.ContactsPresenter
import com.weathen.cureapp.contacts.view.ContactsView
import com.weathen.cureapp.details.activity.DetailsActivity
import com.weathen.cureapp.di.component.DaggerAppComponent
import com.weathen.cureapp.di.modules.ApplicationModule
import com.weathen.cureapp.model.Contact
import javax.inject.Inject

class MainActivity : AppCompatActivity(), ContactsView.Listener {

    var mPresenter: ContactsPresenter? = null
    var mView: ContactsView? = null

    @Inject
    lateinit var dataManager: ContactDataManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as? Application)?.appComponent?.inject(  this)

        mView = ContactsView(layoutInflater, null)
        mPresenter = ContactsPresenter(mView)
        dataManager.loadContacts().observe(this, Observer { contacts ->
            mPresenter?.pushContacts(contacts)
        })

        setContentView(mView?.getRootView())
    }

    override fun onContactClicked(contact: Contact) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.CONTACT_PARAM_EXTRA, contact)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        mView?.registerListener(this)
    }

    override fun onStop() {
        super.onStop()
        mView?.unregisterListener(this)
    }
}
