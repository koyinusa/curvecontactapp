package com.weathen.cureapp.contacts.view

import android.arch.paging.PagedList
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.weathen.cureapp.R
import com.weathen.cureapp.common.BaseObservableViewMvc
import com.weathen.cureapp.common.BaseViewMvc
import com.weathen.cureapp.model.Contact

interface IContactsView {
    fun updateList(contactsPagedList: PagedList<Contact>?)
}

class ContactsView(layoutInflater: LayoutInflater, container: ViewGroup?) : BaseObservableViewMvc<ContactsView.Listener>(), IContactsView {

    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: ContactsAdapter? = null

    init {
        setRootView(layoutInflater.inflate(R.layout.activity_main, container, false))

        mRecyclerView = findViewById(R.id.contact_list)
        mRecyclerView?.layoutManager = LinearLayoutManager(getContext())
        mAdapter = ContactsAdapter(ContactDiffUtilCallback())
        mAdapter?.clickListener = { it -> for (listener in getListeners()) if (it != null) listener.onContactClicked(it) }
        mRecyclerView?.adapter = mAdapter
    }

    override fun updateList(contactsPagedList: PagedList<Contact>?) {
        mAdapter?.submitList(contactsPagedList)
    }

    interface Listener {
        fun onContactClicked(contact: Contact)
    }

}

class ContactDiffUtilCallback : DiffUtil.ItemCallback<Contact>() {
    override fun areItemsTheSame(first: Contact, second: Contact): Boolean {
        return first == second
    }

    override fun areContentsTheSame(first: Contact, second: Contact): Boolean {
        return first == second
    }

}