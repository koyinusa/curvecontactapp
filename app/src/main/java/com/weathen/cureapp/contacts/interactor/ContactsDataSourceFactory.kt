package com.weathen.cureapp.contacts.interactor

import android.arch.paging.DataSource
import android.content.ContentResolver
import com.weathen.cureapp.model.Contact

class ContactsDataSourceFactory(private val contentResolver: ContentResolver) :
    DataSource.Factory<Int, Contact>() {

    override fun create(): DataSource<Int, Contact> {
        return ContactsDataSource(contentResolver)
    }
}
