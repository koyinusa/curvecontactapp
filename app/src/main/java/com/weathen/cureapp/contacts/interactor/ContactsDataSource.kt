package com.weathen.cureapp.contacts.interactor

import android.arch.paging.PositionalDataSource
import android.content.ContentResolver
import android.provider.ContactsContract
import com.weathen.cureapp.model.Contact

class ContactsDataSource(private val contentResolver: ContentResolver) :
        PositionalDataSource<Contact>() {

    companion object {
        private val PROJECTION = arrayOf(
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.LOOKUP_KEY,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_URI,
                ContactsContract.Contacts.HAS_PHONE_NUMBER
        )
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Contact>) {
        callback.onResult(getContacts(params.requestedLoadSize, params.requestedStartPosition), 0)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Contact>) {
        callback.onResult(getContacts(params.loadSize, params.startPosition))
    }

    fun getPhoneNumberForId(id: Long): String? {
        var phone: String? = null
        val cp = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf<String>(id.toString()), null)
        if (cp != null && cp.moveToFirst()) {
            phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
        }
        cp.close()
        return phone
    }

    private fun getContacts(limit: Int, offset: Int): MutableList<Contact> {
        val cursor = contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                PROJECTION,
                null,
                null,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY +
                        " ASC LIMIT " + limit + " OFFSET " + offset
        )
        
        cursor?.moveToFirst()
        val contacts: MutableList<Contact> = mutableListOf()
        while (!cursor.isAfterLast) {
            val id = cursor.getLong(cursor.getColumnIndex(PROJECTION[0]))
            val lookupKey = cursor.getString(cursor.getColumnIndex(PROJECTION[0]))
            val name = cursor.getString(cursor.getColumnIndex(PROJECTION[2]))
            val photo = cursor.getString(cursor.getColumnIndex(PROJECTION[3]))
            val hasPhoneNumber = cursor.getInt(cursor.getColumnIndex(PROJECTION[4])) > 0
            val phoneNumber = if (hasPhoneNumber) getPhoneNumberForId(id) else null //cursor.getString(cursor.getColumnIndex(PROJECTION[5]))
            contacts.add(Contact(id, lookupKey, name, phoneNumber, photo))
            cursor.moveToNext()
        }
        cursor.close()

        return contacts
    }
}