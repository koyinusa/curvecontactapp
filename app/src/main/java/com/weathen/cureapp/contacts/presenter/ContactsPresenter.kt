package com.weathen.cureapp.contacts.presenter

import android.arch.paging.PagedList
import com.weathen.cureapp.common.BaseViewPresenter
import com.weathen.cureapp.contacts.view.IContactsView
import com.weathen.cureapp.model.Contact

interface IContactsPresenter {
    fun pushContacts(contacts: PagedList<Contact>?)
}

class ContactsPresenter(view: IContactsView?) : BaseViewPresenter<IContactsView>(view), IContactsPresenter {

    override fun pushContacts(contacts: PagedList<Contact>?){
        view?.updateList(contacts)
    }

}