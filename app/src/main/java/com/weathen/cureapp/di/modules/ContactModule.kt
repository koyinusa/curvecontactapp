package com.weathen.cureapp.di.modules

import android.content.ContentResolver
import android.content.Context
import com.weathen.cureapp.contacts.interactor.ContactDataManager
import com.weathen.cureapp.contacts.interactor.ContactsDataSourceFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ContactModule(val context: Context) {

    @Singleton
    @Provides
    fun provideContentResolver(): ContentResolver{
        return context.contentResolver
    }

    @Provides
    fun provideDataSourceFactory(contentResolver: ContentResolver) : ContactsDataSourceFactory {
        return ContactsDataSourceFactory(contentResolver)
    }

    @Provides
    fun provideDataManager(dataSourceFactory: ContactsDataSourceFactory) : ContactDataManager {
        return ContactDataManager(dataSourceFactory)
    }

}