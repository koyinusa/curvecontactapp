package com.weathen.cureapp.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ApplicationModule(private val application: Application) {

    internal val applicationContext: Context
        @Singleton
        @Provides
        get() = this.application

    @Provides
    fun provideSharedPreference(context: Context): SharedPreferences {
        return context.getSharedPreferences("hello", Context.MODE_PRIVATE)
    }

}
