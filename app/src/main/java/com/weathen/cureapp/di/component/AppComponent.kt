package com.weathen.cureapp.di.component

import com.weathen.cureapp.contacts.activity.MainActivity
import com.weathen.cureapp.di.modules.ContactModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ContactModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}
