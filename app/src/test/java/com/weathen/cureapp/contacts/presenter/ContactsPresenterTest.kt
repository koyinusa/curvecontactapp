package com.weathen.cureapp.contacts.presenter

import android.arch.paging.PagedList
import com.weathen.cureapp.contacts.view.IContactsView
import com.weathen.cureapp.model.Contact
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ContactsPresenterTest {

    @Captor
    lateinit var ac : ArgumentCaptor<PagedList<Contact>>

    @Mock
    lateinit var pagedList : PagedList<Contact>

    @Mock
    lateinit var contactView : IContactsView

    lateinit var SUT : ContactsPresenter

    @Before
    fun setup(){
        SUT = ContactsPresenter(contactView)
    }

    @Test
    fun pushContacts_success_isPagedListPassedToViewAndIsViewUpdated() {
        SUT.pushContacts(pagedList)
        Mockito.verify(contactView, Mockito.times(1)).updateList(ac.capture())
        assertThat(ac.value, CoreMatchers.`is`(pagedList))
    }
}